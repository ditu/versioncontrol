### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### Api Documentation

# POST Request to add a new data

curl --location 'http://localhost:8080/api/data/2' \
--header 'X-API-KEY: <<API_KEY>>' \
--header 'Content-Type: application/json' \
--data '{
  "productId": 1,
  "appId": 1,
  "env": "production",
  "version": "v1.0",
  "authToken": "yourAuthToken123",
  "timestamp": "2024-01-18T09:30:50",
  "region": "us-west",
  "notes": "new data"
}
'




# GET Request to read data

curl --location --request GET 'http://localhost:8080/api/v1/' \
--header 'X-API-KEY: <<API_KEY>>' \
--header 'Content-Type: application/json' \
--data '{
   
}'