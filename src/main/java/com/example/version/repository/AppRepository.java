package com.example.version.repository;

import com.example.version.model.App;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppRepository extends JpaRepository<App, Long> {
    
    List<App> findByProductId(Long productId);
    
}
