package com.example.version.repository;

import com.example.version.dto.AggregatedDeploymentCountDto;
import com.example.version.model.Data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataRepository extends JpaRepository<Data, Long> {

    List<Data> findByAppId(Long appId);

    @Query("SELECT NEW com.example.version.dto.AggregatedDeploymentCountDto(DATE(d.timestamp), COUNT(d)) FROM Data d GROUP BY DATE(d.timestamp)")
    List<AggregatedDeploymentCountDto> getAggregatedDeploymentCount();
}