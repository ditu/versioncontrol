package com.example.version.controller;

import com.example.version.model.Data;


import java.util.Date;
import com.example.version.dto.*;
import com.example.version.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/data")
public class DataController {

    @Autowired
    private DataRepository dataRepository;

    @GetMapping
    public List<Data> getAllData() {
        return dataRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Data> getDataById(@PathVariable Long id) {
        return dataRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    
    @GetMapping("/by-app/{appId}")
    public ResponseEntity<List<Data>> getDataByAppId(@PathVariable Long appId) {
        List<Data> dataByAppId = dataRepository.findByAppId(appId);
        return ResponseEntity.ok(dataByAppId);
    }
    
    @GetMapping("/aggregated-deployment-count")
    public ResponseEntity<List<AggregatedDeploymentCountDto>> getAggregatedDeploymentCount() {
        List<AggregatedDeploymentCountDto> aggregatedData = dataRepository.getAggregatedDeploymentCount();
        return ResponseEntity.ok(aggregatedData);
    }
    

    @PostMapping
    public ResponseEntity<Data> createData(@RequestBody Data data) {
        // Set the timestamp to the current date and time
        data.setTimestamp(new Date());

        Data savedData = dataRepository.save(data);
        return new ResponseEntity<>(savedData, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Data> updateData(@PathVariable Long id, @RequestBody Data newData) {
        if (dataRepository.existsById(id)) {
            Data existingData = dataRepository.findById(id).orElse(null);

            if (existingData != null) {
                // Update each field with the corresponding value from newData
                existingData.setProductId(newData.getProductId());
                existingData.setAppId(newData.getAppId());
                existingData.setEnv(newData.getEnv());
                existingData.setVersion(newData.getVersion());
                existingData.setAuthToken(newData.getAuthToken());
                existingData.setTimestamp(new Date()); // Set the timestamp to the current date and time
                existingData.setRegion(newData.getRegion());
                existingData.setNotes(newData.getNotes());

                Data updatedData = dataRepository.save(existingData);
                return ResponseEntity.ok(updatedData);
            }
        }

        return ResponseEntity.notFound().build();
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteData(@PathVariable Long id) {
        if (dataRepository.existsById(id)) {
            dataRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}