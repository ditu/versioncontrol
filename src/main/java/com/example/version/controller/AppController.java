package com.example.version.controller;

import com.example.version.model.App;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.example.version.repository.AppRepository;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/")
public class AppController {

    @Autowired
    private AppRepository appRepository;

    @GetMapping
    public List<App> getAllApps() {
        return appRepository.findAll();
    }

    @GetMapping("/apps/{productId}")
    public ResponseEntity<List<Map<String, Object>>> getAppNamesByProductId(@PathVariable Long productId) {
        List<Map<String, Object>> appDetails = appRepository.findByProductId(productId)
                .stream()
                .map(app -> {
                    Map<String, Object> appInfo = new HashMap<>();
                    appInfo.put("appId", app.getId());
                    appInfo.put("appName", app.getAppName());
                    return appInfo;
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok(appDetails);
    }

    // You can add more endpoints based on your needs

    // Example endpoint to get a single app by ID
    @GetMapping("/{id}")
    public ResponseEntity<App> getAppById(@PathVariable Long id) {
        return appRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    // Example endpoint to create a new app
    @PostMapping("/apps")
    public ResponseEntity<App> createApp(@RequestBody App app) {
        App savedApp = appRepository.save(app);
        return ResponseEntity.ok(savedApp);
    }

    // Example endpoint to update an existing app
    @PutMapping("/{id}")
    public ResponseEntity<App> updateApp(@PathVariable Long id, @RequestBody App app) {
        if (appRepository.existsById(id)) {
            app.setId(id);
            App updatedApp = appRepository.save(app);
            return ResponseEntity.ok(updatedApp);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Example endpoint to delete an app by ID
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteApp(@PathVariable Long id) {
        if (appRepository.existsById(id)) {
            appRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}