package com.example.version;

import org.springframework.boot.*;

import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
@EntityScan("com.example.version.model")
public class DemoApplication {
	
	@GetMapping("/")
	public String home() {
		return "200";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
