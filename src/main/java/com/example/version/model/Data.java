package com.example.version.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "data")
public class Data {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "app_id")
    private Long appId;

    private String env;
    private String version;

    @Column(name = "auth_token")
    private String authToken;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    private String region;
    private String notes;

    // Constructors

    public Data() {
        // Default constructor needed by JPA
    }

    public Data(Long productId, Long appId, String env, String version, String authToken,
                Date timestamp, String region, String notes) {
        this.productId = productId;
        this.appId = appId;
        this.env = env;
        this.version = version;
        this.authToken = authToken;
        this.timestamp = timestamp;
        this.region = region;
        this.notes = notes;
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}