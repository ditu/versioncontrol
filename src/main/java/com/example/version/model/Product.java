package com.example.version.model;

import javax.persistence.*;

@Entity
@Table(name = "products") // Specify the table name in the database
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "dev_team")
    private String devTeam;

    private String notes;

    // Constructors

    public Product() {
        // Default constructor needed by JPA
    }

    public Product(String productName, String devTeam, String notes) {
        this.productName = productName;
        this.devTeam = devTeam;
        this.notes = notes;
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDevTeam() {
        return devTeam;
    }

    public void setDevTeam(String devTeam) {
        this.devTeam = devTeam;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
