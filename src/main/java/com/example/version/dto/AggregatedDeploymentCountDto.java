package com.example.version.dto;

import java.util.Date;

public class AggregatedDeploymentCountDto {
    private Date date;
    private long count;

    public AggregatedDeploymentCountDto(Date date, long count) {
        this.date = date;
        this.count = count;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}